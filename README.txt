# Copyright (C) Mov.ai  - All Rights Reserved
#    Unauthorized copying of this file, via any medium is strictly prohibited
#    Proprietary and confidential
#    Developers:
#    - Lucia Carreira - 08/2020

#
# This package implements the Localization functions for Mov.ai.
#
